CREATE TABLE public.category
(
    id bigint NOT NULL,
    name character varying(124),
    category_parent_id bigint,
    is_deleted boolean DEFAULT false,
    PRIMARY KEY (id)
);

ALTER TABLE public.category
    ADD CONSTRAINT category_id_key FOREIGN KEY (category_parent_id)
    REFERENCES public.category (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;

CREATE TABLE public.product
(
    id bigint NOT NULL,
    cdate timestamp without time zone,
    udate timestamp without time zone,
    name text,
    description text,
    category_id bigint,
    currency character varying(16),
    price double precision,
    is_deleted boolean DEFAULT false,
    PRIMARY KEY (id),
    CONSTRAINT category_key FOREIGN KEY (category_id)
        REFERENCES public.category (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

CREATE TABLE public.attribute
(
    id bigint NOT NULL,
    name character varying(124),
    PRIMARY KEY (id)
);

CREATE TABLE public.category_attribute
(
    category_id bigint,
    attribute_id bigint,
    CONSTRAINT categoty_key FOREIGN KEY (category_id)
        REFERENCES public.category (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT attribute_key FOREIGN KEY (attribute_id)
        REFERENCES public.attribute (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

CREATE TABLE public.product_attribute
(
 	id bigint NOT NULL,
    product_id bigint,
    attribute_id bigint,
    value character varying(256),
    PRIMARY KEY (id),
    CONSTRAINT product_key FOREIGN KEY (product_id)
        REFERENCES public.product (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT attribute_key FOREIGN KEY (attribute_id)
        REFERENCES public.attribute (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);
