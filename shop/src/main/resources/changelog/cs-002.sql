INSERT INTO public.category(id, name)	VALUES (1, 'Electronics');

INSERT INTO public.category(id, name, category_parent_id)	VALUES (2, 'Camera & Photo', 1);   
INSERT INTO public.category(id, name, category_parent_id)	VALUES (3, 'Computers & Accessories', 1);
INSERT INTO public.category(id, name, category_parent_id)	VALUES (4, 'Television & Video', 1);

INSERT INTO public.category(id, name, category_parent_id)	VALUES (5, 'Accessories', 2);   
INSERT INTO public.category(id, name, category_parent_id)	VALUES (6, 'Tripods & Monopods', 2);
INSERT INTO public.category(id, name, category_parent_id)	VALUES (7, 'Filters & Accessories', 2);

INSERT INTO public.category(id, name, category_parent_id)	VALUES (8, 'Desktops', 3);   
INSERT INTO public.category(id, name, category_parent_id)	VALUES (9, 'Laptops', 3);
INSERT INTO public.category(id, name, category_parent_id)	VALUES (10, 'Tablets', 3);

INSERT INTO public.category(id, name, category_parent_id)	VALUES (11, 'Televisions', 4);   
INSERT INTO public.category(id, name, category_parent_id)	VALUES (12, 'Projectors', 4);
INSERT INTO public.category(id, name, category_parent_id)	VALUES (13, 'Blu-ray Players & Recorders', 4);