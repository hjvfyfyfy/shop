CREATE SEQUENCE public.product_attribute_seq START 100;
CREATE SEQUENCE public.category_seq START 100;
CREATE SEQUENCE public.attribute_seq START 100;
CREATE SEQUENCE public.product_seq START 100;
