INSERT INTO public.product_attribute(id, product_id, attribute_id, value)	VALUES (1, 1, 1, 'VESA Wall Mount Pattern 100mm (V) x 100mm (H)');
INSERT INTO public.product_attribute(id, product_id, attribute_id, value)	VALUES (2, 1, 2, 'TCL');
INSERT INTO public.product_attribute(id, product_id, attribute_id, value)	VALUES (3, 1, 3, '720p');
INSERT INTO public.product_attribute(id, product_id, attribute_id, value)	VALUES (4, 1, 4, 'Wireless');

INSERT INTO public.product_attribute(id, product_id, attribute_id, value)	VALUES (5, 2, 2, 'SAMSUNG');
INSERT INTO public.product_attribute(id, product_id, attribute_id, value)	VALUES (6, 2, 3, '720p');
INSERT INTO public.product_attribute(id, product_id, attribute_id, value)	VALUES (7, 2, 4, 'Wi-Fi');

INSERT INTO public.product_attribute(id, product_id, attribute_id, value)	VALUES (8, 3, 2, 'SAMSUNG');
INSERT INTO public.product_attribute(id, product_id, attribute_id, value)	VALUES (9, 3, 3, '1080p');
INSERT INTO public.product_attribute(id, product_id, attribute_id, value)	VALUES (10, 3, 4, 'Wi-Fi');

INSERT INTO public.product_attribute(id, product_id, attribute_id, value)	VALUES (11, 4, 2, 'Ohderii');
INSERT INTO public.product_attribute(id, product_id, attribute_id, value)	VALUES (12, 4, 5, 'VGA, USB');
INSERT INTO public.product_attribute(id, product_id, attribute_id, value)	VALUES (13, 4, 6, '1920 x 1080');

INSERT INTO public.product_attribute(id, product_id, attribute_id, value)	VALUES (14, 5, 2, 'BRILENS');
INSERT INTO public.product_attribute(id, product_id, attribute_id, value)	VALUES (15, 5, 5, 'VGA, TransFlash, USB, HDMI');

INSERT INTO public.product_attribute(id, product_id, attribute_id, value)	VALUES (16, 6, 2, 'Vankyo');
INSERT INTO public.product_attribute(id, product_id, attribute_id, value)	VALUES (17, 6, 5, 'VGA, USB, HDMI');
INSERT INTO public.product_attribute(id, product_id, attribute_id, value)	VALUES (18, 6, 6, '1080P Supported');

