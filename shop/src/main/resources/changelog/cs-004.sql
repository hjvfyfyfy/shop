INSERT INTO public.attribute(id, name) VALUES (1, 'Mounting Type');
INSERT INTO public.attribute(id, name) VALUES (2, 'Brand');
INSERT INTO public.attribute(id, name) VALUES (3, 'Resolution');
INSERT INTO public.attribute(id, name) VALUES (4, 'Connections');
INSERT INTO public.attribute(id, name) VALUES (5, 'Hardware Interface');
INSERT INTO public.attribute(id, name) VALUES (6, 'Display Resolution');

INSERT INTO public.category_attribute(category_id, attribute_id) VALUES (11, 1);
INSERT INTO public.category_attribute(category_id, attribute_id) VALUES (11, 2);
INSERT INTO public.category_attribute(category_id, attribute_id) VALUES (11, 3);
INSERT INTO public.category_attribute(category_id, attribute_id) VALUES (11, 4);
INSERT INTO public.category_attribute(category_id, attribute_id) VALUES (12, 2);
INSERT INTO public.category_attribute(category_id, attribute_id) VALUES (12, 5);
INSERT INTO public.category_attribute(category_id, attribute_id) VALUES (12, 6);