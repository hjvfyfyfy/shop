package ua.small.project.shop.service.impl;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import lombok.extern.slf4j.Slf4j;
import ua.small.project.shop.dto.FixerIoResponse;
import ua.small.project.shop.enums.Currency;
import ua.small.project.shop.service.ExchangeService;

@Slf4j
@Service
public class ExchangeServiceImpl implements ExchangeService {
	Cache<Currency, Double> cache = CacheBuilder.newBuilder().maximumSize(1000).expireAfterWrite(10, TimeUnit.MINUTES).build();

	@Autowired
	private RestTemplate restTemplate;

	@Value("${fixer.io.key}")
	private String key;
	@Value("${fixer.io.url}")
	private String url;

	@Override
	public double getExchangeRate(Currency currency) {
		Double exchangeRate = cache.getIfPresent(currency);
		if(exchangeRate != null) {
			log.info("exchangeRate was uploaded from the cache, currency = " + currency);
			return exchangeRate;
		}
		FixerIoResponse response = restTemplate.getForObject(url + "/latest?access_key=" + key + "&symbols=" + currency.name(), FixerIoResponse.class);
		if (!response.isSuccess())
			throw new RuntimeException("fixer.io is not available");
		
		exchangeRate = response.getRates().get(currency.name());
		
		log.info("push to the cache, currency = " + currency);
		cache.put(currency, exchangeRate);
		return response.getRates().get(currency.name());
	}
}
