package ua.small.project.shop.enums;

public enum Currency {
	USD("Dollar"),
	EUR("Euro"),
	JPY("Japanese Yen"),
	GBP("Great British Pound"),
	CAD("Canadian Dollar"),
	CHF("Swiss Franc");
	
	private String fullName;

	public String getFullName() {
		return fullName;
	}

	private Currency(String fullName) {
		this.fullName = fullName;
	}
}
