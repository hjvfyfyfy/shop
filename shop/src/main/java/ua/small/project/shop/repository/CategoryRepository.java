package ua.small.project.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ua.small.project.shop.model.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long>{

}
