package ua.small.project.shop.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class ProductAttributeDto {
	
	@NotNull(groups = {CreateGroup.class, UpdateGroup.class})
	private Long attributeId;

	private String attributeName;
	
	@NotNull(groups = {CreateGroup.class, UpdateGroup.class})
	@Size(max = 256, groups = {CreateGroup.class, UpdateGroup.class})
	private String value;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductAttributeDto other = (ProductAttributeDto) obj;
		if (attributeId == null) {
			if (other.attributeId != null)
				return false;
		} else if (!attributeId.equals(other.attributeId))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attributeId == null) ? 0 : attributeId.hashCode());
		return result;
	}
}
