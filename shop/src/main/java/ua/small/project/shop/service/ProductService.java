package ua.small.project.shop.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import ua.small.project.shop.model.Product;

public interface ProductService {

	Optional<Product> get(Long id);

	Product save(Product product);

	void delete(Long id);

	Page<Product> getAll(Pageable pageable);

}
