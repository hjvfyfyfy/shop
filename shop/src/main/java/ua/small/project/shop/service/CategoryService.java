package ua.small.project.shop.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import ua.small.project.shop.model.Category;

public interface CategoryService {

	Optional<Category> get(Long id);

	void delete(Long id);

	Category save(Category category);

	Page<Category> getAll(Pageable pageable);

}
