package ua.small.project.shop.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ua.small.project.shop.model.ProductAttribute;

public interface ProductAttributeRepository extends JpaRepository<ProductAttribute, Long> {

	@Transactional
	@Modifying
	@Query(value = "DELETE ProductAttribute pa WHERE pa.product.id = :id")
	void deleteByProductId(@Param("id") Long id);
}