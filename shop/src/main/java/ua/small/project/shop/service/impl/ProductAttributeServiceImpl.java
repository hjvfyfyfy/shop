package ua.small.project.shop.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.small.project.shop.model.ProductAttribute;
import ua.small.project.shop.repository.ProductAttributeRepository;
import ua.small.project.shop.service.ProductAttributeService;

@Service
public class ProductAttributeServiceImpl implements ProductAttributeService{

	@Autowired
	private ProductAttributeRepository productAttributeRepository;
	
	@Override
	public void deleteByProductId(Long id) {
		productAttributeRepository.deleteByProductId(id);
	}

	@Override
	public ProductAttribute save(ProductAttribute productAttribute) {
		return productAttributeRepository.save(productAttribute);
	}

	@Override
	public Optional<ProductAttribute> getById(Long id) {
		return productAttributeRepository.findById(id);
	}
}