package ua.small.project.shop.service;

import ua.small.project.shop.dto.CategoryDto;
import ua.small.project.shop.dto.ProductDto;
import ua.small.project.shop.model.Category;
import ua.small.project.shop.model.Product;

public interface MappingService {
	
	ProductDto mapProductToProductDto (Product product, String currency);
	
	Product mapProductDtoToProduct(ProductDto productDto);
	
	void mapProductDtoToProduct(ProductDto productDto, Product product);

	Category mapCategoryDtoToCategory(CategoryDto categoryDto);

	void mapCategoryDtoToCategory(CategoryDto categoryDto, Category category);
	
	
}