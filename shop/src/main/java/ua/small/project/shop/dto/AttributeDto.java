package ua.small.project.shop.dto;

import lombok.Data;

@Data
public class AttributeDto {

	private Long id;
	
	private String name;
}