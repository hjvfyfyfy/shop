package ua.small.project.shop.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
@Table(name = "product_attribute")
public class ProductAttribute {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_attribute_generator")
	@SequenceGenerator(name="product_attribute_generator", sequenceName = "product_attribute_seq", allocationSize = 1)
	private Long id;
	
	@ToString.Exclude
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;
	
	@ManyToOne
	@JoinColumn(name = "attribute_id")
	private Attribute attribute;

	private String value;
}