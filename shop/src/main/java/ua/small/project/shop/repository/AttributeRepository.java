package ua.small.project.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ua.small.project.shop.model.Attribute;

@Repository
public interface AttributeRepository extends JpaRepository<Attribute, Long>{

}
