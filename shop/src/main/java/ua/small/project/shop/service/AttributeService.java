package ua.small.project.shop.service;

import java.util.Optional;

import ua.small.project.shop.model.Attribute;

public interface AttributeService {

	Optional<Attribute> getById(Long attributeId);
}
