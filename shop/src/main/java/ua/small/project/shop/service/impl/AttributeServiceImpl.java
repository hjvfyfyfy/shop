package ua.small.project.shop.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.small.project.shop.model.Attribute;
import ua.small.project.shop.repository.AttributeRepository;
import ua.small.project.shop.service.AttributeService;

@Service
public class AttributeServiceImpl implements AttributeService{

	@Autowired
	private AttributeRepository attributeRepository;
	
	@Override
	public Optional<Attribute> getById(Long attributeId) {
		return attributeRepository.findById(attributeId);
	}
}