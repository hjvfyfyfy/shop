package ua.small.project.shop.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
public class Attribute {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "attribute_generator")
	@SequenceGenerator(name="attribute_generator", sequenceName = "attribute_seq", allocationSize = 1)
	private Long id;

	private String name;

	@ToString.Exclude
	@JsonIgnore
	@ManyToMany(mappedBy = "attributes")
	private List<Category> categories;
}