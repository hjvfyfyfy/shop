package ua.small.project.shop.validation;

import java.util.Date;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class ErrorMessage {
	private int statusCode;
	private Date timestamp;
	private Map<String, String> errors;
}