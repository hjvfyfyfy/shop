package ua.small.project.shop.service;

import java.util.Optional;

import ua.small.project.shop.model.ProductAttribute;

public interface ProductAttributeService {

	void deleteByProductId(Long id);

	ProductAttribute save(ProductAttribute productAttribute);

	Optional<ProductAttribute> getById(Long id);

}
