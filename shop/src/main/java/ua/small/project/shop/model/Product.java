package ua.small.project.shop.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

import lombok.Data;
import ua.small.project.shop.enums.Currency;

@Data
@Entity
@SQLDelete(sql = "UPDATE Product SET is_deleted = true WHERE id = ?")
@Where(clause = "is_deleted = false")
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_generator")
	@SequenceGenerator(name="product_generator", sequenceName = "product_seq", allocationSize = 1)
	private Long id;

	@CreationTimestamp
	private Date cdate;
	
	@UpdateTimestamp
	private Date udate;

    private String name;
    
	private String description;
    
    @ManyToOne
	@JoinColumn(name = "category_id")
    private Category category;
    
    @Enumerated(EnumType.STRING)
    private Currency currency;

	private double price;
    
	@OneToMany(mappedBy = "product", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<ProductAttribute> productAttributes;
	
	@PreUpdate
	@PrePersist
	void prePersist() {
		productAttributes.stream().forEach(x -> x.setProduct(this));
	}
}
