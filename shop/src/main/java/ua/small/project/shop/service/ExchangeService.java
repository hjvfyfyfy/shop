package ua.small.project.shop.service;

import ua.small.project.shop.enums.Currency;

public interface ExchangeService {
	
	double getExchangeRate(Currency currency);
}
