package ua.small.project.shop.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.small.project.shop.dto.CategoryDto;
import ua.small.project.shop.dto.ProductAttributeDto;
import ua.small.project.shop.dto.ProductDto;
import ua.small.project.shop.enums.Currency;
import ua.small.project.shop.model.Attribute;
import ua.small.project.shop.model.Category;
import ua.small.project.shop.model.Product;
import ua.small.project.shop.model.ProductAttribute;
import ua.small.project.shop.service.AttributeService;
import ua.small.project.shop.service.CategoryService;
import ua.small.project.shop.service.ExchangeService;
import ua.small.project.shop.service.MappingService;
import ua.small.project.shop.service.ProductAttributeService;

@Service
public class MappingServiceImpl implements MappingService{

	@Autowired
	private Mapper mapper;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private ProductAttributeService productAttributeService;
	@Autowired
	private AttributeService attributeService;
	@Autowired
	private ExchangeService exchangeService;
	
	@Override
	public ProductDto mapProductToProductDto(Product product, String currency) {
		ProductDto productDto = mapper.map(product, ProductDto.class);
		if(StringUtils.isNotBlank(currency)) {
			Double exchangeRate = exchangeService.getExchangeRate(Currency.valueOf(currency));
			productDto.setCurrency(Currency.valueOf(currency));
			productDto.setPrice(productDto.getPrice() * exchangeRate);
		}
		return productDto;
	}
	
	@Override
	public Product mapProductDtoToProduct(ProductDto productDto) {
		Product product = mapper.map(productDto, Product.class);
		mapExtraPropToProduct(productDto, product);
		return product;
	}

	@Override
	public void mapProductDtoToProduct(ProductDto productDto, Product product) {
		mapper.map(productDto, product);
		mapExtraPropToProduct(productDto, product);
	}
	
	private void mapExtraPropToProduct(ProductDto productDto, Product product) {
		Category category = categoryService.get(productDto.getCategoryId()).orElseThrow(() -> new RuntimeException("CategoryId is not exists"));
		product.setCategory(category);
		
		double exchangeRate = exchangeService.getExchangeRate(productDto.getCurrency());
		product.setPrice(product.getPrice() / exchangeRate);
		product.setCurrency(Currency.EUR);
		
		if(product.getId() != null) {
			productAttributeService.deleteByProductId(product.getId());
		}

		List<ProductAttribute> productAttributes = mapListPAttributeDtoToListPAttribute(productDto.getProductAttributes());
		product.setProductAttributes(productAttributes);
	}
	
	public List<ProductAttribute> mapListPAttributeDtoToListPAttribute (List<ProductAttributeDto> productAttributeDto) {
		return productAttributeDto.parallelStream().distinct().map(x -> mapPAttributeDtoToPAttribute(x)).collect(Collectors.toList());
	}
	
	public ProductAttribute mapPAttributeDtoToPAttribute (ProductAttributeDto productAttributeDto) {
		ProductAttribute productAttribute = new ProductAttribute();
		
		Optional<Attribute> attribute = attributeService.getById(productAttributeDto.getAttributeId());
		productAttribute.setAttribute(attribute.orElseThrow(() -> new RuntimeException("AttributeId is not exists")));
		
		productAttribute.setValue(productAttributeDto.getValue());
		return productAttribute;
	}

	@Override
	public Category mapCategoryDtoToCategory(CategoryDto categoryDto) {
		Category category = mapper.map(categoryDto, Category.class);
		category.setCategoryParent(categoryService.get(categoryDto.getParentId()).orElseThrow(() -> new RuntimeException("ParentId is not exists")));
		return category;
	}

	@Override
	public void mapCategoryDtoToCategory(CategoryDto categoryDto, Category category) {
		mapper.map(categoryDto, category);
		category.setCategoryParent(categoryService.get(categoryDto.getParentId()).orElseThrow(() -> new RuntimeException("ParentId is not exists")));
	}
}