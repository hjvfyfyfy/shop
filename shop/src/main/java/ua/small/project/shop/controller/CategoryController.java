package ua.small.project.shop.controller;

import java.util.Optional;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import ua.small.project.shop.dto.BaseResponse;
import ua.small.project.shop.dto.CategoryDto;
import ua.small.project.shop.dto.CreateGroup;
import ua.small.project.shop.dto.UpdateGroup;
import ua.small.project.shop.model.Category;
import ua.small.project.shop.service.CategoryService;
import ua.small.project.shop.service.MappingService;

@Slf4j
@RestController
@RequestMapping(path = "/categories")
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private Mapper mapper;
	@Autowired
	private MappingService mappingService;

	@RequestMapping(path = "/", method = RequestMethod.GET)
	public ResponseEntity<Page<CategoryDto>> getAllCategory(Pageable pageable) {
		Page<Category> categories = categoryService.getAll(pageable);
		return new ResponseEntity<>(categories.map(x -> mapper.map(x, CategoryDto.class)), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<BaseResponse> getCategory(@PathVariable Long id) {
		Optional<Category> category = categoryService.get(id);
		if(category.isPresent()) {
			CategoryDto categoryDto = mapper.map(category.get(), CategoryDto.class);
			return new ResponseEntity<BaseResponse>(categoryDto, HttpStatus.OK);
		}
		return new ResponseEntity<BaseResponse>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Object> createCategory(@Validated(CreateGroup.class) @RequestBody CategoryDto categoryDto) {
		Category category = mappingService.mapCategoryDtoToCategory(categoryDto);
		try {
			category = categoryService.save(category);
			return new ResponseEntity<>(category.getId(), HttpStatus.OK);
		} catch (Exception e) {
			log.error("createCategory", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Object> updateCategory(@Validated(UpdateGroup.class) @RequestBody CategoryDto categoryDto) {
		try {
			Optional<Category> category = categoryService.get(categoryDto.getId());
			if(category.isEmpty()) {
				return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
			}
			mappingService.mapCategoryDtoToCategory(categoryDto, category.get());
		
			categoryService.save(category.get());
			return new ResponseEntity<Object>(HttpStatus.OK);
		} catch (Exception e) {
			log.error("updateCategory", e);
			return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<BaseResponse> deleteCategory(@PathVariable Long id) {
		try {
			categoryService.delete(id);
			return new ResponseEntity<BaseResponse>(HttpStatus.OK);
		} catch (Exception e) {
			log.error("deleteCategory", e);
			return new ResponseEntity<BaseResponse>(HttpStatus.NOT_FOUND);
		}
	}
}