package ua.small.project.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ua.small.project.shop.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>{

}
