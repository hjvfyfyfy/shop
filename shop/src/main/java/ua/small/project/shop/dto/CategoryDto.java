package ua.small.project.shop.dto;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class CategoryDto extends BaseResponse {
	
	@Null(groups = CreateGroup.class)
	@NotNull(groups = UpdateGroup.class)
	private Long id;
	
	@NotNull(groups = {CreateGroup.class, UpdateGroup.class})
	@Size(max = 124, groups = {CreateGroup.class, UpdateGroup.class})
	private String name;

	private Long parentId;
}
