package ua.small.project.shop.dto;

import java.util.Map;

import lombok.Data;

@Data
public class FixerIoResponse {
	private boolean success;
	private int timestamp;
	private String base;
	private String date;
	private Map<String, Double> rates;
}
