package ua.small.project.shop.dto;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import lombok.Data;
import ua.small.project.shop.enums.Currency;

@Data
public class ProductDto extends BaseResponse {
	
	@Null(groups = CreateGroup.class)
	@NotNull(groups = UpdateGroup.class)
	private Long id;
	
	private Date cdate;
	
	private Date udate;

	@NotNull(groups = {CreateGroup.class, UpdateGroup.class})
	private Long categoryId;

	@NotNull(groups = {CreateGroup.class, UpdateGroup.class})
	private String category;
	
	@NotNull(groups = {CreateGroup.class, UpdateGroup.class})
    private String name;
    
    @NotNull(groups = {CreateGroup.class, UpdateGroup.class})
	private String description;
	
	@NotNull(groups = {CreateGroup.class, UpdateGroup.class})
	private Double price;

	@NotNull(groups = {CreateGroup.class, UpdateGroup.class})
	private Currency currency;
	
	private List<ProductAttributeDto> productAttributes;
}
