package ua.small.project.shop.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import ua.small.project.shop.dto.BaseResponse;
import ua.small.project.shop.dto.CreateGroup;
import ua.small.project.shop.dto.ProductDto;
import ua.small.project.shop.dto.UpdateGroup;
import ua.small.project.shop.model.Product;
import ua.small.project.shop.service.MappingService;
import ua.small.project.shop.service.ProductService;

@Slf4j
@RestController
@RequestMapping(path = "/products")
public class ProductController {

	@Autowired
	private ProductService productService;
	@Autowired
	private MappingService mappingService;

	@RequestMapping(path = "/", method = RequestMethod.GET)
	public ResponseEntity<Page<ProductDto>> getAllProduct(@RequestParam(required = false) String currency, Pageable pageable) {
		Page<Product> products = productService.getAll(pageable);
		return new ResponseEntity<>(products.map(x -> mappingService.mapProductToProductDto(x, currency)), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<BaseResponse> getProduct(@PathVariable Long id, @RequestParam(required = false) String currency) {
		Optional<Product> product = productService.get(id);
		if (product.isPresent()) {
			ProductDto productDto = mappingService.mapProductToProductDto(product.get(), currency);
			return new ResponseEntity<BaseResponse>(productDto, HttpStatus.OK);
		}
		return new ResponseEntity<BaseResponse>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Object> createProduct(@Validated(value = CreateGroup.class) @RequestBody ProductDto productDto) {
		try {
			Product product = mappingService.mapProductDtoToProduct(productDto);

			product = productService.save(product);
			return new ResponseEntity<Object>(product.getId(), HttpStatus.OK);
		} catch (Exception e) {
			log.error("createProduct", e);
			return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Object> updateProduct(@Validated(value = UpdateGroup.class) @RequestBody ProductDto productDto) {
		try {
			Optional<Product> optProduct = productService.get(productDto.getId());
			if (optProduct.isEmpty()) {
				return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
			}
			Product product = optProduct.get();
			mappingService.mapProductDtoToProduct(productDto, product);

			productService.save(product);
			return new ResponseEntity<Object>(HttpStatus.OK);
		} catch (Exception e) {
			log.error("updateProduct", e);
			return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<BaseResponse> deleteProduct(@PathVariable Long id) {
		try {
			productService.delete(id);
			return new ResponseEntity<BaseResponse>(HttpStatus.OK);
		} catch (Exception e) {
			log.error("deleteProduct", e);
			return new ResponseEntity<BaseResponse>(HttpStatus.NOT_FOUND);
		}
	}
}
