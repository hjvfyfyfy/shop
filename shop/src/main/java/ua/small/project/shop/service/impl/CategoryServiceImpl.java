package ua.small.project.shop.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ua.small.project.shop.model.Category;
import ua.small.project.shop.repository.CategoryRepository;
import ua.small.project.shop.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService{

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Override
	public Optional<Category> get(Long id) {
		return categoryRepository.findById(id);
	}

	@Override
	public Category save(Category category) {
		return categoryRepository.save(category);
	}
	
	@Override
	public void delete(Long id) {
		categoryRepository.deleteById(id);
	}

	@Override
	public Page<Category> getAll(Pageable pageable) {
		return categoryRepository.findAll(pageable);
	}
}
